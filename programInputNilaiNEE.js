var readline = require('readline');
var util = require('util');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

var question = util.promisify(rl.question).bind(rl);
async function ask() {
  var siswaCount = await question('jumlah siswa: ');
  var nilai = [];
    for(var i = 0;i<siswaCount;i++){
        try {
            var getNilai = await question(`nilai siswa ke ${i+1}: ` );
            nilai.push(getNilai)
        } catch (err) {
            console.error('Invalid Input', err);
        }
        
    }
    console.log(siswaCount);
    console.log(nilai);

    //tertinggi, terrendah
console.log("Nilai tertinggi : ");

console.log(Math.max(...nilai));
console.log("Nilai terendah : ");

console.log(Math.min(...nilai));
rl.close();


//mengurutkan nilai terrendah ke tertinggi
nilai.sort((a, b) => {
    return a - b;
    
});
console.log("urutan terendah ke tertinggi...")
console.log(nilai);

//mencari rata rata

function average(nilai){

    var total = 0;
    nilai.forEach(function(rata) {
    total += Number(rata);
    });

    var avg = total/nilai.length;
    return parseFloat(avg).toFixed(1);
    }
    console.log("Nilai rata-rata : ");

    console.log(average(nilai));

//jumlah siswa lulus dan tidak lulus
    console.log("Jumlah siswa lulus / tidak lulus ujian")

const kelulusan = (nilai) => {
  let countLulus = 0;
  let countTidakLulus = 0;
  for (let i = 0; i < nilai.length; i++) {
    if (nilai[i] >= 75) {
      countLulus++;
    } else if (nilai[i] < 75) {
      countTidakLulus++;
    }
  }
  return `Siswa Lulus = ${countLulus}. Siswa Tidak Lulus = ${countTidakLulus}`;
};

console.log(kelulusan(nilai));
    rl.close();
}

ask();
